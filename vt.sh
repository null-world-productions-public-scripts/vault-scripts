#!/usr/bin/env sh

vault_token() {
    export VAULT_TOKEN="$(vault login -method oidc -token-only)"
}
vault_token
echo $VAULT_TOKEN
