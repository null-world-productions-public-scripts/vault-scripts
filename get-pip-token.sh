#!/usr/bin/env sh
#
# Get a Pip Token for Gitlab from Vault.
#
# Previously, the Docker Builder was given a Vault Token, and used that token
# to query Vault for the Pip Token.
#
# The problem was that the RUN lines had begun by exporting the PIP_TOKEN
# variable by doing a curl request, which busted the cache on that line every
# time. The solution, I believe, is to set the PIP_TOKEN as a build arg instaed
# of the VAULT_TOKEN. That way there's no dynamic curling done on the big RUN
# layers.
#
# ENVIRONMENT VARIABLES
# ---------------------
# VAULT_TOKEN
#   Vault token generated from a policy with access to the secrets path for this
#   project
# VAULT_URL
#   The url to reach the desired instance of vault
# VAULT_PIP_TOKEN_PATH
#   The key in vault to retrieve the required pip token from
#

installed() { which "$1" >/dev/null 2>&1; }
err() { echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2; }
die() { >&2 echo "Fatal: ${*}"; exit 1; }

installed "curl" || die "Missing 'curl'"
installed "jq" || die "Missing 'jq'"

assert_not_empty() {
  arg_name="$1"
  arg_value="$2"

  if [ -z "$arg_value" ]; then
    echo "The value for '$arg_name' cannot be empty"
    exit 1
  fi
}

assert_not_empty "VAULT_TOKEN" "${VAULT_TOKEN}"

PIP_TOKEN=$(curl -s -X GET "${VAULT_URL}/v1/secret/data/${VAULT_PIP_TOKEN_PATH}" \
    -H  "accept: */*" \
    -H  "X-Vault-Token: ${VAULT_TOKEN}" \
    | jq -r '.data.data.PIP_TOKEN')
echo "${PIP_TOKEN}"

# vim: tw=80 sw=2
